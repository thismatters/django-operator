from copy import deepcopy

import kopf

from django_operator.services import (
    DeploymentService,
    HorizontalPodAutoscalerService,
    IngressService,
    PodService,
    ServiceService,
)
from django_operator.utils import merge, slugify, superget


class DjangoKind:
    kind_services = {
        "pod": PodService,
        "ingress": IngressService,
        "service": ServiceService,
        "deployment": DeploymentService,
        "horizontalpodautoscaler": HorizontalPodAutoscalerService,
    }

    def __init__(self, *, logger, patch, body, spec, status, namespace, **_):
        self.logger = logger
        try:
            host = spec["host"]
            version = spec["version"]
        except KeyError:
            patch.status["condition"] = "degraded"
            kopf.exception(body, reason="ConfigError", message="")
            raise kopf.PermanentError("Spec missing required field")

        version_slug = slugify(version)

        self.base_kwargs = {
            "host": host,
            "version": version,
            "version_slug": version_slug,
        }
        self.body = body
        self.host = host
        self.spec = spec
        self.patch = patch
        self.status = status
        self.version = version
        self.namespace = namespace
        self.version_slug = version_slug

    class DeploymentNotEnabled(Exception):
        pass

    def spec_for_deployment(self, *, purpose, _raise=None):
        common = deepcopy(superget(self.spec, "deployments.common", default={}))
        if purpose != "common":
            merge(
                common,
                superget(
                    self.spec,
                    f"deployments.{purpose}",
                    _raise=_raise,
                    default={},
                ),
            )
        return common

    def read_resource(self, kind, purpose, name):
        kind_service_class = self.kind_services[kind]
        obj = kind_service_class(logger=self.logger).read(
            namespace=self.namespace,
            name=name,
        )
        return obj

    def delete_resource(self, *, kind, name):
        return self._ensure(kind=kind, purpose="purpose", existing=name, delete=True)

    def unprotect_resource(self, *, kind, name):
        kind_service_class = self.kind_services[kind]
        kind_service_class(logger=self.logger).unprotect(
            namespace=self.namespace,
            name=name,
        )

    def _ensure_raw(
        self, kind, purpose, delete=False, template=None, parent=None, **kwargs
    ):
        kind_service_class = self.kind_services[kind]
        if template is None:
            template = f"{kind}.yaml"
        if parent is None:
            parent = self.body
        obj = kind_service_class(logger=self.logger).ensure(
            namespace=self.namespace,
            template=template,
            purpose=purpose,
            parent=parent,
            delete=delete,
            **kwargs,
            **self.base_kwargs,
        )
        return obj

    def _ensure(self, kind, purpose, delete=False, **kwargs):
        obj = self._ensure_raw(kind, purpose, delete=delete, **kwargs)
        if not delete:
            return {kind: {purpose: obj.metadata.name}}
        return {}

    def pod_phase(self, name):
        pod = PodService(logger=self.logger).read_status(
            namespace=self.namespace, name=name
        )
        return pod.status.phase.lower()

    def deployment_reached_condition(self, *, name, condition):
        self.logger.debug(f"within deployment_reached_condition: name= {name}")
        deployment = DeploymentService(logger=self.logger).read_status(
            namespace=self.namespace, name=name
        )
        if deployment.status.conditions is None:
            return False
        for _condition in deployment.status.conditions:
            if _condition.type == condition:
                return _condition.status == "True"
        return False

    def ensure_redis(self):
        ret = self._ensure(
            kind="deployment",
            purpose="redis",
            template="deployment_redis.yaml",
            delete=not superget(self.spec, "redis.enabled", default=False),
            existing=superget(self.status, "created.deployment.redis"),
            image=superget(self.spec, "redis.image"),
            redis_port=superget(self.spec, "redis.port"),
            enrichments={
                "spec": {
                    "template": {
                        "spec": {
                            ("containers", 0): {
                                "resources": superget(self.spec, "redis.resources"),
                            }
                        }
                    }
                }
            },
        )
        merge(
            ret,
            self._ensure(
                kind="service",
                purpose="redis",
                template="service_redis.yaml",
                delete=not superget(self.spec, "redis.enabled", default=False),
                existing=superget(self.status, "created.service.redis"),
                redis_port=superget(self.spec, "redis.port"),
                service_name=superget(self.spec, "redis.serviceName", default="redis"),
            ),
        )
        return ret

    def start_manage_commands(self):
        manage_commands = superget(self.spec, "manageCommands.commands", default=[])
        if manage_commands:
            return self.ensure_manage_commands(manage_commands=manage_commands)

    def ensure_manage_commands(self, *, manage_commands):
        enriched_commands = []
        _spec = self.spec_for_deployment(purpose="common")
        env_from = self._get_env_from(spec=_spec)
        for manage_command in manage_commands:
            _manage_command = "-".join(manage_command)
            enriched_commands.append(
                {
                    "name": slugify(_manage_command),
                    "image": self._get_image_from(spec=_spec),
                    "command": ["python", "manage.py"] + manage_command,
                    "env": _spec.get("env", []),
                    "envFrom": env_from,
                    "volumeMounts": _spec.get("volumeMounts", []),
                }
            )
        enrichments = {
            "spec": {
                "imagePullSecrets": _spec.get("imagePullSecrets", []),
                "volumes": _spec.get("volumes", []),
                "initContainers": enriched_commands,
            }
        }

        _pod = self._ensure(
            kind="pod",
            purpose="migrations",
            template="pod_migrations.yaml",
            enrichments=enrichments,
        )
        return superget(_pod, "pod.migrations")

    def clean_manage_commands(self, *, pod_name):
        # delete the pod
        self.delete_resource(kind="pod", name=pod_name)

    def _resource_names(self, *, kind, purpose):
        existing = superget(self.status, f"created.{kind}.{purpose}", default="")

        # see if the version changed
        if existing and existing.endswith(self.version_slug):
            former = None
        else:
            former = existing
            existing = None
        return former, existing

    def _migrate_resource(
        self,
        *,
        purpose,
        enrichments=None,
        kind="deployment",
        template=None,
        skip_delete=False,
        **kwargs,
    ):
        enabled = True
        try:
            _spec = self.spec_for_deployment(
                purpose=purpose, _raise=self.DeploymentNotEnabled
            )
        except self.DeploymentNotEnabled:
            self.logger.info(f"Deployment for {purpose} is not enabled, skipping.")
            enabled = False
        blue_name, green_name = self._resource_names(
            kind=kind,
            purpose=purpose,
        )
        self.logger.debug(
            f"migrate {purpose} {kind} => former = {blue_name} :: "
            f"existing = {green_name} :: skip_delete = {skip_delete}"
        )

        ret = {}
        if enabled:
            # bring up the green deployment
            green_obj = self._ensure_raw(
                kind="deployment",
                purpose=purpose,
                enrichments=enrichments,
                existing=green_name,
                template=template,
                **kwargs,
            )
            ret = {kind: {purpose: green_obj.metadata.name}}

            if kind == "deployment":
                # create horizontal pod autoscaling if appropriate
                hpa_details = superget(_spec, "autoscaler", default={})
                if hpa_details.get("enabled", False):
                    hpa_kwargs = {
                        "deployment_name": green_obj.metadata.name,
                        "cpu_threshold": hpa_details["cpuUtilizationThreshold"],
                        "max_replicas": superget(hpa_details, "replicas.maximum"),
                        "min_replicas": superget(hpa_details, "replicas.minimum"),
                        "current_replicas": green_obj.spec.replicas,
                    }

                    if blue_name:
                        blue_obj = DeploymentService(logger=self.logger).read(
                            namespace=self.namespace,
                            name=blue_name,
                        )
                        hpa_kwargs.update({"current_replicas": blue_obj.spec.replicas})
                    merge(
                        ret,
                        self._ensure(
                            kind="horizontalpodautoscaler",
                            purpose=purpose,
                            template="horizontalpodautoscaler.yaml",
                            parent=green_obj,
                            **hpa_kwargs,
                        ),
                    )

        # bring down the blue_obj deployment
        if blue_name and not skip_delete:
            self.logger.debug(f"migrate {purpose} => doing delete")
            self._ensure(
                kind="deployment",
                purpose=purpose,
                existing=blue_name,
                delete=True,
            )
        return ret

    def _get_env_from(self, *, spec):
        env_from = []
        for config_map_name in spec.get("envFromConfigMapRefs", []):
            env_from.append({"configMapRef": {"name": config_map_name}})
        for config_map_name in spec.get("envFromSecretRefs", []):
            env_from.append({"secretRef": {"name": config_map_name}})
        return env_from

    def _get_image_from(self, *, spec):
        return f"{spec['image']}:{self.version}"

    def __base_enrichments(self, *, spec, purpose):
        env_from = self._get_env_from(spec=spec)
        enrichments = {
            "imagePullSecrets": spec.get("imagePullSecrets", []),
            "volumes": spec.get("volumes", []),
            ("containers", 0): {
                "image": self._get_image_from(spec=spec),
                "command": superget(
                    spec,
                    "command",
                    _raise=kopf.PermanentError(f"missing {purpose} command"),
                ),
                "args": superget(spec, "args", default=[]),
                "env": spec.get("env", []),
                "envFrom": env_from,
                "volumeMounts": spec.get("volumeMounts", []),
                "resources": spec.get("resources", []),
                "ports": spec.get("ports", []),
            },
        }
        probe_spec = spec.get("probeSpec", None)
        if probe_spec is not None:
            enrichments[("containers", 0)].update(
                {
                    "livenessProbe": probe_spec,
                    "readinessProbe": probe_spec,
                }
            )
        if superget(self.spec, "redis.enabled", default=False):
            enrichments.update(
                {
                    "initContainers": [
                        {
                            "name": "init-broker",
                            "image": "busybox:1.28.4",
                            "command": [
                                "sh",
                                "-c",
                                "until nslookup redis; do sleep 1; done;",
                            ],
                        }
                    ]
                }
            )
        return enrichments

    def _base_enrichments(self, *, spec, purpose):
        try:
            _spec = self.spec_for_deployment(
                purpose=purpose, _raise=self.DeploymentNotEnabled
            )
        except self.DeploymentNotEnabled:
            return {}
        return {
            "spec": {
                "strategy": _spec.get("strategy", {}),
                "template": {
                    "spec": self.__base_enrichments(spec=_spec, purpose=purpose)
                },
            }
        }

    def start_green(self, *, purpose):
        enrichments = self._base_enrichments(spec=self.spec, purpose=purpose)
        return self._migrate_resource(
            purpose=purpose,
            enrichments=enrichments,
            skip_delete=True,
        )

    def clean_blue(self, *, purpose, blue):
        if blue:
            self.logger.debug(f"migrate {purpose} => doing delete")
            self.delete_resource(kind="deployment", name=blue)

    def migrate_worker(self):
        # worker data gathering
        return self._migrate_resource(
            purpose="worker",
            enrichments=self._base_enrichments(spec=self.spec, purpose="worker"),
        )

    def migrate_beat(self):
        # beat data gathering
        return self._migrate_resource(
            purpose="beat",
            enrichments=self._base_enrichments(spec=self.spec, purpose="beat"),
        )

    def migrate_service(self):
        ret = self._ensure(
            kind="service",
            purpose="app",
            service_type=superget(self.spec, "services.app.type", default="ClusterIP"),
            app_port=superget(
                self.spec_for_deployment(purpose="app"),
                "ports.0.containerPort",
            ),
        )

        # create Ingress
        _annotations = superget(self.spec, "ingresses.app.annotations", default=[])
        merge(
            ret,
            self._ensure(
                kind="ingress",
                purpose="app",
                enrichments={
                    "metadata": {
                        "annotations": {a["name"]: a["value"] for a in _annotations}
                    },
                    "spec": {
                        "ingressClassName": superget(
                            self.spec, "ingresses.app.ingressClassName", default="nginx"
                        ),
                        "tls": superget(self.spec, "ingresses.app.tls", default=[]),
                    },
                },
            ),
        )
        return ret
