# Django Operator

**This is severely alpha software, so use it at your own risk. There is no warranty, etc. I am looking for someone willing to review/vet the code. If you're interested please reach out by creating an issue!**

A [Kubernetes operator](https://github.com/cncf/tag-app-delivery/blob/main/operator-wg/whitepaper/Operator-WhitePaper_v1-0.md) for a Django stack including Celery, django-celery-beat, and redis.

Provides a `Django` CRD which:
* Coordinates deployment of app
  * creates ingresses for app
  * ensures one beat (`django-celery-beat`)
  * scales app and worker containers independently
* Coordinates initialization of app proper
  * handles migrations
  * handles fixtures
  * handles initialization management commands

Takes some inspiration from [21h/django-operator](https://git.blindage.org/21h/django-operator) which is presented as a freestanding operator written in `go`.

Prior to use you will need to install the django operator onto your cluster with:
```
kubectl apply -f https://gitlab.com/thismatters/django-operator/-/raw/main/django-operator.yml
```

See [sample.yaml](sample.yaml) for a sample manifest for a django app.

## Changing Kubernetes Versions

Annoyingly the Kubernetes people release a new version pretty regularly and drop support for versions just as often, so _this very code_ needs to be updated periodially (as often as I upgrade my cluster!)! Steps below:

* Choose your new k8s version, say `1.28`.
* Go to the [Kubernetes client for Python compatability table](https://github.com/kubernetes-client/python#compatibility), and find which version aligns. (Hint: it's just the minor version of k8s, so in this case `28`.)
* Read [changelog for `kopf`](https://github.com/nolar/kopf/releases), and update code accordingly.
* Update `requirements.txt` to use newest patch of chosen version for `kubernetes` and whatever version of `kopf` is appropriate.
* Not unreasonable to update other dependencies too!
* Run linting and tests:
  * `make build && make run && make prelint && make lint && make test`

## TODO:

* [x] Update secrets in staging, prod once tested
* [x] Figure out global pattern (https://github.com/nolar/kopf/issues/876) -- monolithic!
* [x] write CRD
* [x] create manifests for:
  * [x] deployment -- app
  * [x] deployment -- worker
  * [x] deployment -- beat
  * [x] deployment -- redis
  * [x] ingress -- app
  * [x] service -- redis
  * [x] service -- app
  * [x] job -- migrations
* [x] write create/update the code
* [x] more logging, send events
* [x] write delete code (shouldn't be much here really...)
* [x] set up CI pipeline
* [x] lint
* [x] deploy to cluster (for testing)
* [x] test (create a new namespace for testing, and use an arbitrary URL)
* [x] test updating (don't re-run migrations unless version changes )
* [x] deploy

### v0.1.0
* [x] better logging
* [x] de-monolith the process
* [x] labels ("migration-step": "green-app") are contaminating the ingresses
* [x] incorporate metrics from metric server into autoscaling. (use horizontal pod autoscaling instead!)
* [x] unittests! -- could probably still use more tho
* [x] Explore a more declarative pattern
* [x] more active monitoring of child resources (prevent deletion, &c.) -- daemon to check for the presence of the each `created` thing.
* [x] Better tracking of objects as they are created for garbage collection in event of failed migration, more robust cleanup should include garbage collection -- requires expanded process
* [ ] documentation for users
* [x] Consider restructuring the CRD to have sections for each purpose (`{"app": {"xyz": "val", "abc": "val"}}` as opposed to `{"xyz": {"app": "val", "worker": "val"}}`) -- See [sample-v1alpha2](sample-v1alpha2.yaml) to understand the new structure; making this change requires having a [conversion webhook](https://kubernetes.io/docs/tasks/extend-kubernetes/custom-resources/custom-resource-definition-versioning/#write-a-conversion-webhook-server), I'm reaching out to see if hosting that is possible with kopf. If it is not possible I will use the go template provided in the link.


* [ ] Incoroprate apex redirector ingresses
* [ ] When deployment is _new_ create the ingresses first! (so that certs and dns and all that can get started)



### v0.2.0
* [ ] better rollback process when version migration fails:
  * [ ] detect last db migration in existing deployment
  * [ ] retain the migration pod for troubleshooting
  * [ ] roll back db migrations to prior good state
  * [ ] clean up manifest to reflect prior deployment
* [ ] allow other manifests (deployments, ingresses, services) to be set in django manifest
* [ ] manage a database; to facilitate smoke-test deployments -- allow deployment to be defined in the django manifest


